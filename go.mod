module gitlab.com/gitlab-org/security-products/analyzers/phpcs-security-audit/v2

require (
	github.com/sirupsen/logrus v1.9.3
	github.com/stretchr/testify v1.8.4
	github.com/urfave/cli/v2 v2.25.5
	gitlab.com/gitlab-org/security-products/analyzers/command/v2 v2.2.0
	gitlab.com/gitlab-org/security-products/analyzers/common/v3 v3.2.2
	gitlab.com/gitlab-org/security-products/analyzers/phpcs-security-audit v1.3.0
	gitlab.com/gitlab-org/security-products/analyzers/report/v4 v4.1.2
	gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2 v2.0.2
)

require (
	github.com/Microsoft/go-winio v0.5.2 // indirect
	github.com/ProtonMail/go-crypto v0.0.0-20220711121315-1fde58898e96 // indirect
	github.com/acomagu/bufpipe v1.0.3 // indirect
	github.com/bmatcuk/doublestar v1.3.4 // indirect
	github.com/cpuguy83/go-md2man/v2 v2.0.2 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/emirpasic/gods v1.18.1 // indirect
	github.com/go-git/gcfg v1.5.0 // indirect
	github.com/go-git/go-billy/v5 v5.3.1 // indirect
	github.com/go-git/go-git/v5 v5.4.2 // indirect
	github.com/imdario/mergo v0.3.13 // indirect
	github.com/jbenet/go-context v0.0.0-20150711004518-d14ea06fba99 // indirect
	github.com/kevinburke/ssh_config v1.2.0 // indirect
	github.com/mitchellh/go-homedir v1.1.0 // indirect
	github.com/otiai10/copy v1.7.0 // indirect
	github.com/pelletier/go-toml v1.9.5 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/sergi/go-diff v1.2.0 // indirect
	github.com/urfave/cli v1.20.0 // indirect
	github.com/xanzy/ssh-agent v0.3.1 // indirect
	github.com/xrash/smetrics v0.0.0-20201216005158-039620a65673 // indirect
	gitlab.com/gitlab-org/security-products/analyzers/common v1.6.0 // indirect
	gitlab.com/gitlab-org/security-products/analyzers/ruleset v1.4.1 // indirect
	golang.org/x/crypto v0.0.0-20220622213112-05595931fe9d // indirect
	golang.org/x/net v0.0.0-20220708220712-1185a9018129 // indirect
	golang.org/x/sys v0.0.0-20220715151400-c0bba94af5f8 // indirect
	gopkg.in/warnings.v0 v0.1.2 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

go 1.19
