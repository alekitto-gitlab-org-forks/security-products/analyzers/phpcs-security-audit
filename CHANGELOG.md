# phpcs-security-audit changelog

## v4.0.399
- Update analyzer.yaml reference in ci script

## v4.0.3
- upgrade `github.com/sirupsen/logrus` version [`v1.9.0` => [`v1.9.3`](https://github.com/sirupsen/logrus/releases/tag/v1.9.3)] (gitlab-org/security-products/analyzers/phpcs-security-audit!86)
- upgrade `github.com/stretchr/testify` version [`v1.8.2` => [`v1.8.4`](https://github.com/stretchr/testify/releases/tag/v1.8.4)] (gitlab-org/security-products/analyzers/phpcs-security-audit!86)
- upgrade `github.com/urfave/cli/v2` version [`v2.25.3` => [`v2.25.5`](https://github.com/urfave/cli/releases/tag/v2.25.5)] (gitlab-org/security-products/analyzers/phpcs-security-audit!86)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command/v2` version [`v2.1.0` => [`v2.2.0`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v2.2.0)] (gitlab-org/security-products/analyzers/phpcs-security-audit!86)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v4.1.0` => [`v4.1.2`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.1.2)] (gitlab-org/security-products/analyzers/phpcs-security-audit!86)

## v4.0.2
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset` version [`v1.4.1` => [`v2.0.2`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v2.0.2)] (!85)

## v4.0.1
- upgrade `github.com/urfave/cli/v2` version [`v2.25.1` => [`v2.25.3`](https://github.com/urfave/cli/releases/tag/v2.25.3)] (!84)

## v4.0.0
- Bump to next major version (!83)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command/v2` version [`v1.10.2` => [`v2.1.0`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v2.1.0)] (!83)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v4` version [`v3.22.1` => [`v4.1.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v4.1.0)] (!83)

## v3.3.6
- upgrade Go to v1.19 (!82)
- upgrade `github.com/stretchr/testify` version [`v1.8.1` => [`v1.8.2`](https://github.com/stretchr/testify/releases/tag/v1.8.2)] (!82)
- upgrade `github.com/urfave/cli/v2` version [`v2.23.7` => [`v2.25.1`](https://github.com/urfave/cli/releases/tag/v2.25.1)] (!82)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.10.1` => [`v1.10.2`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.10.2)] (!82)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.17.0` => [`v3.22.1`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.22.1)] (!82)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/ruleset` version [`v1.4.0` => [`v1.4.1`](https://gitlab.com/gitlab-org/security-products/analyzers/ruleset/-/releases/v1.4.1)] (!82)

## v3.3.5
- upgrade `github.com/urfave/cli/v2` version [`v2.23.6` => [`v2.23.7`](https://github.com/urfave/cli/releases/tag/v2.23.7)] (!78)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.10.0` => [`v1.10.1`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.10.1)] (!78)

## v3.3.4
- upgrade `github.com/urfave/cli/v2` version [`v2.23.5` => [`v2.23.6`](https://github.com/urfave/cli/releases/tag/v2.23.6)] (!76)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.16.0` => [`v3.17.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.17.0)] (!76)

## v3.3.3
- Ensure `git` is installed in the Docker image (!75)

## v3.3.2
- Increase the php-cli memory limit to unlimited so large files can be scanned (!74)
  - This is the same behaviour pre-v3.3.1.

## v3.3.1
- Reduce the size of the analyzer image by ~40MB (!73)
- upgrade `github.com/stretchr/testify` version [`v1.8.0` => [`v1.8.1`](https://github.com/stretchr/testify/releases/tag/v1.8.1)] (!73)
- upgrade `github.com/urfave/cli/v2` version [`v2.19.2` => [`v2.23.5`](https://github.com/urfave/cli/releases/tag/v2.23.5)] (!73)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.9.2` => [`v1.10.0`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.10.0)] (!73)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.15.2` => [`v3.16.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.16.0)] (!73)

## v3.3.0
- upgrade `github.com/urfave/cli/v2` version [`v2.11.1` => [`v2.19.2`](https://github.com/urfave/cli/releases/tag/v2.19.2)] (!72)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.9.1` => [`v1.9.2`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.9.2)] (!72)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/common/v3` version [`v3.2.0` => [`v3.2.2`](https://gitlab.com/gitlab-org/security-products/analyzers/common/-/releases/v3.2.2)] (!72)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.13.0` => [`v3.15.2`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.15.2)] (!72)

## v3.2.0
- upgrade `github.com/sirupsen/logrus` version [`v1.8.1` => [`v1.9.0`](https://github.com/sirupsen/logrus/releases/tag/v1.9.0)] (!70)
- upgrade `github.com/stretchr/testify` version [`v1.7.0` => [`v1.8.0`](https://github.com/stretchr/testify/releases/tag/v1.8.0)] (!70)
- upgrade `github.com/urfave/cli/v2` version [`v2.11.0` => [`v2.11.1`](https://github.com/urfave/cli/releases/tag/v2.11.1)] (!70)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/command` version [`v1.8.2` => [`v1.9.1`](https://gitlab.com/gitlab-org/security-products/analyzers/command/-/releases/v1.9.1)] (!70)
- upgrade `gitlab.com/gitlab-org/security-products/analyzers/report/v3` version [`v3.12.2` => [`v3.13.0`](https://gitlab.com/gitlab-org/security-products/analyzers/report/-/releases/v3.13.0)] (!70)

## v3.1.1
- Upgrade the `command` package for better analyzer messages (!69)

## v3.1.0
- Upgrade core analyzer dependencies (!68)
  + Adds support for globstar patterns when excluding paths
  + Adds analyzer details to the scan report

## v3.0.0
- Bumping to `v3.0.0` (!66)

## v2.12.1
- Update command to `v1.6.1` (!64)
- Update report to `v3.10.0` (!64)
- Update crypto to latest (!64)

## v2.12.0
- Update ruleset, report, and command modules to support ruleset overrides (!63)

## v2.11.3
- Update common to `v2.24.1` which fixes a git certificate error when using `ADDITIONAL_CA_CERT_BUNDLE` (!61)

## v2.11.2
- Upgrade to to v1.17 (!60)

## v2.11.1
- Update alpine linux container packages as part of Docker build (!57)

## v2.11.0
- Update report dependency in order to use the report schema version 14.0.0 (!52)

## v2.10.1
- Update common to `v2.22.1` which fixes a CA Certificate bug when analyzer is run more than once (!50)

## v2.10.0
- Update common to v2.22.0 (!49)
- Update urfave/cli to v2.3.0 (!49)

## v2.9.1
- Update composer to v2.0.7 (!48)
- Update logrus, testify, cli, and common golang dependencies to latest versions

## v2.9.0
- Update common and enable disablement of rulesets (!46)

## v2.8.2
- Fix bug which prevented writing `ADDITIONAL_CA_CERT_BUNDLE` value to `/etc/gitconfig` (!41)

## v2.8.1
- Update golang dependencies (!40)

## v2.8.0
- Add `scan.start_time`, `scan.end_time` and `scan.status` to report (!37)

## v2.7.1
- Upgrade go to version 1.15 (!36)

## v2.7.0
- Add scan object to report (!32)

## v2.6.0
- Switch to the MIT Expat license (!29)

## v2.5.0
- Update logging to be standardized across analyzers (!28)

## v2.4.3
- Remove `location.dependency` from the generated SAST report (!27)

## v2.4.2
- Update phpcs-security-audit to 2.0.1 (!25)

## v2.4.1
- Fix Docker image to allow php user to write to custom CA bundle file (!26)

## v2.4.0
- Added New CLI Flags:  **extensions** -- Comma seperated list of additional PHP file extensions to pass into the processor (!20)

## v2.3.1
- Use upstream Composer base image (!21)

## v2.3.0
- Add description to issue (!19)

## v2.2.0
- Add `id` field to vulnerabilities in JSON report (!18)

## v2.1.0
- Add support for custom CA certs (!16)

## v2.0.1
- Update common to v2.1.6

## v2.0.0
- Switch to new report syntax with `version` field

## v1.3.0
- Add `Scanner` property and deprecate `Tool`

## v1.2.0
- Show command error output

## v1.1.0
- Enrich report with more data

## v1.0.0
- initial release
