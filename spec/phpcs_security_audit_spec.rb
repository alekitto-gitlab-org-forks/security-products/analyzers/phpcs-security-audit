require "tmpdir"
require "English"

require 'gitlab_secure/integration_test/docker_runner'
require 'gitlab_secure/integration_test/shared_examples/scan_shared_examples'
require 'gitlab_secure/integration_test/shared_examples/report_shared_examples'
require 'gitlab_secure/integration_test/spec_helper'

describe 'running image' do
  let(:fixtures_dir) { 'qa/fixtures' }
  let(:expectations_dir) { 'qa/expect' }

  def project_dir
     '/app'
  end

  def image_name
    ENV.fetch('TMP_IMAGE', 'phpcs-security-audit:latest')
  end

  def privileged
    ENV.fetch('PRIVILEGED', 'true') == 'true'
  end

  context 'with no project' do
    before(:context) do
      @output = `docker run -t --rm -w #{project_dir} #{image_name}`
      @exit_code = $CHILD_STATUS.to_i
    end

    it 'shows there is no match' do
      expect(@output).to match(/no match in #{project_dir}/i)
    end

    describe 'exit code' do
      specify { expect(@exit_code).to be 0 }
    end
  end

  # rubocop:disable RSpec/MultipleMemoizedHelpers
  context 'with test project' do
    def parse_expected_report(expectation_name, report_name = 'gl-sast-report.json')
      path = File.join(expectations_dir, expectation_name, report_name)
      JSON.parse(File.read(path))
    end

    let(:global_vars) do
      {
        'ANALYZER_INDENT_REPORT': 'true',
        # CI_PROJECT_DIR env var value is referred by the Analyzer(1) and
        # Post Analyzer script(2) to determine the target directory for
        # performing any action.
        #
        # References:
        # (1): https://gitlab.com/gitlab-org/security-products/analyzers/command/blob/6eb84053950dcf4c06b768484880f9d3d9aebde1/run.go#L132-132
        # (2): https://gitlab.com/gitlab-org/security-products/post-analyzers/scripts/-/blob/25479eae03e423cd67f2493f23d0c4f9789cdd0e/start.sh#L2
        'CI_PROJECT_DIR': project_dir
      }
    end

    let(:project) { 'any' }
    let(:variables) { {} }
    let(:command) { [] }
    let(:script) { nil }
    let(:offline) { false }
    let(:target_dir) { File.join(fixtures_dir, project) }

    let(:scan) do
      GitlabSecure::IntegrationTest::DockerRunner.run_with_cache(
        image_name, fixtures_dir, target_dir, @description,
        command: command,
        script: script,
        offline: offline,
        privileged: privileged,
        variables: global_vars.merge(variables),
        report_filename: 'gl-sast-report.json')
    end

    let(:report) { scan.report }

    context "simple php project" do
      let(:project) { "php-composer" }
      let(:variables) do
          { 'GITLAB_FEATURES': 'vulnerability_finding_signatures' }
      end

      it_behaves_like "successful scan"

      describe "created report" do
        it_behaves_like "non-empty report"

        it_behaves_like "recorded report" do
          let(:recorded_report) { parse_expected_report(project) }
        end

        it_behaves_like "valid report"
      end
    end

    context "additional cert php project" do
      let(:project) { "php-composer" }
      let(:variables) do
          { 'GITLAB_FEATURES': 'vulnerability_finding_signatures',
            'ADDITIONAL_CA_CERT_BUNDLE': '-----BEGIN CERTIFICATE-----
        certificate-contents-go-here
        -----END CERTIFICATE-----'}
      end

      it_behaves_like "successful scan"

      describe "created report" do
        it_behaves_like "non-empty report"

        it_behaves_like "recorded report" do
          let(:recorded_report) { parse_expected_report(project) }
        end

        it_behaves_like "valid report"
      end
    end
  end
end
