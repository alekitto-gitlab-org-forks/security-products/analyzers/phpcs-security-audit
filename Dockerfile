FROM golang:1.19-alpine AS build

ENV CGO_ENABLED=0 GOOS=linux

WORKDIR /go/src/buildapp
COPY . .
# build the analyzer binary and automatically set the AnalyzerVersion
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
    PATH_TO_MODULE=`go list -m` && \
    go build -ldflags="-X '$PATH_TO_MODULE/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o /analyzer

FROM php:alpine

COPY --from=composer/composer:2-bin /composer /usr/bin/composer

RUN apk update && \
    apk upgrade --no-cache && \
    apk add --no-cache git

# Set unlimited max memory. The composer base image we previously used did
# this out of the box, but we switched to a different base image to reduce
# the overall size: https://gitlab.com/gitlab-org/security-products/analyzers/phpcs-security-audit/-/merge_requests/73
RUN echo "memory_limit=-1" >> /usr/local/etc/php/conf.d/gitlab-sast.ini

# Create php user and group
RUN addgroup -g 1000 php && \
    adduser -u 1000 -D -h /home/php -G php php

# The php user below doesn't have permission to create a file in /etc/ssl/certs,
# this RUN command creates files that the analyzer can write to.
RUN mkdir -p /etc/ssl/certs/ && \
    touch /etc/ssl/certs/ca-certificates.crt && \
    chown root:php /etc/ssl/certs/ca-certificates.crt && \
    chmod g+w /etc/ssl/certs/ca-certificates.crt

USER php:php
WORKDIR /home/php/

ARG SCANNER_VERSION
ENV SCANNER_VERSION ${SCANNER_VERSION:-2.0.1}

# Install phpcs-security-audit and add standards to PHP CodeSniffer
RUN composer require --dev pheromone/phpcs-security-audit $SCANNER_VERSION && \
    mv vendor/pheromone/phpcs-security-audit/Security vendor/squizlabs/php_codesniffer/src/Standards/ && \
    mv vendor/pheromone/phpcs-security-audit/example_base_ruleset.xml . && \
    mv vendor/pheromone/phpcs-security-audit/example_drupal7_ruleset.xml . && \
    mv vendor/pheromone/phpcs-security-audit/tests.php .

COPY --chown=php:php ruleset.xml .

COPY --from=build /analyzer /analyzer

ENTRYPOINT []
CMD ["/analyzer", "run"]
