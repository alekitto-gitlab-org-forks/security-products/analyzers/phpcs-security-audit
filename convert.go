package main

import (
	"encoding/json"
	"io"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/phpcs-security-audit/v2/metadata"
	report "gitlab.com/gitlab-org/security-products/analyzers/report/v4"
	ruleset "gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2"
)

func convert(reader io.Reader, prependPath string) (*report.Report, error) {
	var newReport Report

	err := json.NewDecoder(reader).Decode(&newReport)
	if err != nil {
		return nil, err
	}

	// HACK: extract root path from environment variables
	root := os.Getenv("ANALYZER_TARGET_DIR")
	if root == "" {
		root = os.Getenv("CI_PROJECT_DIR")
	}

	// Process PHP CodeSniffer report
	vulns := []report.Vulnerability{}
	for path, fileReport := range newReport.Files {
		for _, m := range fileReport.Messages {
			rel := filepath.Join(prependPath, strings.TrimPrefix(path, root))
			vulns = append(vulns, report.Vulnerability{
				Category:    metadata.Type,
				Scanner:     &metadata.IssueScanner,
				Name:        m.Message,
				Message:     m.Message,
				CompareKey:  m.CompareKey(rel),
				Severity:    m.Severity(),
				Location:    m.Location(rel),
				Identifiers: m.Identifiers(),
				Description: m.Message,
			})
		}
	}

	var dsReport = report.NewReport()
	dsReport.Analyzer = metadata.AnalyzerID
	dsReport.Config.Path = ruleset.PathSAST
	dsReport.Vulnerabilities = vulns
	return &dsReport, nil
}

// Report is a report returned by PHP CodeSniffer.
type Report struct {
	Files map[string]FileReport
}

// FileReport is the section of a report for one specific source file.
type FileReport struct {
	Messages []Message
}

// Message is a vulnerability found in the source code.
// It's either an error or a warning message.
type Message struct {
	// Fixable bool // Fixable is always false
	Column  int
	Source  string // Source refers to a class like PHPCS_SecurityAudit.BadFunctions.EasyXSS.EasyXSSwarn
	Message string
	// Severity int // Severity is always 5
	Type string // Type is either WARNING or ERROR
	Line int
}

// CompareKey returns a string used to establish whether two issues are the same.
// It takes the relative path of the affected file since it's not known in this context.
func (m Message) CompareKey(filepath string) string {
	return strings.Join([]string{filepath, m.Source}, ":") // NOTE: no line number
}

// Severity returns the normalized severity.
func (m Message) Severity() report.SeverityLevel {
	switch m.Type {
	case "ERROR":
		return report.SeverityLevelHigh
	case "WARNING":
		return report.SeverityLevelLow
	}
	return report.SeverityLevelUnknown
}

// Location returns a structured Location
func (m Message) Location(rel string) report.Location {
	return report.Location{
		File:      rel,
		LineStart: m.Line,
	}
}

// Identifiers returns the normalized identifiers of the vulnerability.
func (m Message) Identifiers() []report.Identifier {
	return []report.Identifier{
		m.PSAIdentifier(),
	}
}

// PSAIdentifier returns a structured Identifier for a PHPCS_SecurityAudit source type
// ex: PHPCS_SecurityAudit.BadFunctions.PregReplace.PregReplaceE
func (m Message) PSAIdentifier() report.Identifier {
	return report.Identifier{
		Type:  "phpcs_security_audit_source",
		Name:  m.Source,
		Value: m.Source,
	}
}
